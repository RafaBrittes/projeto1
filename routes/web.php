<?php


/** @var \Laravel\Lumen\Routing\Router $router */


$router->get('/', function(){
    return view('pages/home');
});

$router->get('/sobre', function(){
    return view('pages/sobre');
});

$router->group(['prefix' => "/users"], function () use ($router){
    $router->get('/',       'ExampleController@mostrarTodos');
    $router->get("/{id}",   'ExampleController@showUsers');
    $router->post('/',      'ExampleController@userCreate'); 
    $router->put("/{id}",   'ExampleController@userUpdate');
    $router->delete("/{id}",'ExampleController@userDelete');
});


$router->get("/getmenu/{restaurant}", 'ExampleController@getInfoMultipedidos');

$router->post('register', 'AuthController@register');
$router->post('login',    'AuthController@login');

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router){
    $router->get('logout', 'AuthController@logout');  
    $router->get('showmyinfo', 'AuthController@showMyInfo');
});