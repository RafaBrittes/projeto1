<?php
use Faker\Generator as Faker;

$factory->define(usersModels::class, function(Faker $faker){
    return [
        'name' => $faker->name,
        'email' => $faker->email
    ];
});