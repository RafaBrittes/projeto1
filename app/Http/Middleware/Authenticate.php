<?php

namespace App\Http\Middleware;

use Closure;
use Firebase\JWT\JWT as FirebaseJWT;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $bearerToken = $this->request->bearerToken();

        if (

            FirebaseJWT::decode($bearerToken, env('JWT_SECRET'), ['HS256'])

        ) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}