<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\usersModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Firebase\JWT\JWT as FirebaseJWT;



class AuthController extends Controller
{
    public function register(Request $request){
        $this->validate($request, [
            'name' =>     'required|unique:users,name',
            'email' =>    'required|confirmed',
            'password' => 'required|confirmed'
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $password = Hash::make($request->input('password'));


        usersModels::create([
            'name' =>      $name,
            'email' =>     $email,
            'password' =>  $password
        ]);

        return response()->json(['status' => 'success', 'operation' => 'created']);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['name', 'email', 'password']);

        // pega email dele 
        // verificar se o email existe no banco de dados
        // se o email existir, coleta o HASH da senha,
        // após coletar o hash da senha, verifica se a senha enviada confere com o hash
        // Hash::check('senha', 'hash')
        // retornar um jwt

        /* 
        
        $token = FirebaseJWT::encode([
            'iss' => "lumen-jwt",
            'sub' => $userID,
            'iat' => time(),
            'exp' => time() + 60*60
        ], env('JWT_SECRET'));
        
        return ['token' => $token];
        */
    }
}