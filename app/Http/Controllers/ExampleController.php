<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Models\usersModels;

class ExampleController extends Controller
{
    private $model;
 
    public function __construct(usersModels $users)
    {
        $this->model = $users;
    }

    public function mostrarTodos(){
        $users = $this->model->all();
        return response()->json($users);
    }

    public function showUsers($id){
        $users = $this->model->find($id);
        return response()->json($users);

    //$users = app('db')->select("SELECT * from users WHERE id= $id");
    }

    public function userCreate(Request $request){
        $users = $this->model->create($request->all());
        return response()->json($users);
        //       app('db')->insert("id", $name, $email);
    }

    public function userUpdate($id, Request $request){
        $users = $this->model->find($id)->update($request->all());
        return response()->json($users);

    }

    public function userDelete($id){
        $users = $this->model->find($id)->delete();
        return response()->json(null);
    }

    public function getInfoMultipedidos($restaurant){
        $resp= Http::get("https://app.multipedidos.com.br/server/restaurant/data/$restaurant");
        return print_r($resp->json());
    }

    public function returnValues(){
        $collection = Http::get("https://app.multipedidos.com.br/server/restaurant/data/superpizza");
        return view('sobre',['collection'=>$collection['data']]);
    }

};