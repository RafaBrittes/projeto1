<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC&display=swap" rel="stylesheet">

</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Teste</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="/sobre">Sobre</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link1</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link2</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link3</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Opção 1</a></li>
            <li><a class="dropdown-item" href="#">Opção 2</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Opção 3</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Opção 4</a></li>
            <li><a class="dropdown-item" href="#">Opção 5</a></li>
          </ul>
        </li>
      </ul>
      <div class="btn-group" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-dark">Dark</button>
      <button type="button" class="btn btn-secondary">Grey</button>
      <button type="button" class="btn btn-light">White</button>
      </div>
      <label for="customRange3" class="form-label" style="margin-left:10px">Tamanho da Letra</label>
      <input type="range" class="form-range" min="0" max="2" step="1" id="customRange3" style="width:50px; margin-left:5px">
    </div>
  </div>
</nav>
</body>
@yield('content')

<footer>  
  <div class="container-fluid foot">
  <div class="row">
    <div class="col lista">
    <ul>
    <li>Rua Blabla, 123</li>
    <li>Bairro Blabla</li>
    <hr>
    <li>Telefones:</li>
    <li>(47) 5544-4455</li>
    <li>(47) 9 9775-5047</li>
    </ul>
    </div>
    <div class="col">
    <ul>
      <li>Horário de atendimento:</li>
      <li>Segunda-feira: 09:00 - 23:00</li>
      <li>Terça-feira: 09:00 - 23:00</li>
    </ul>
    </div>
  </div>
  </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>