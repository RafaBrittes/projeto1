import { ReturnStatement } from '@angular/compiler';
import { Component, Input, OnChanges } from '@angular/core';

import { DeployerService } from '@app/_services';

declare const Pusher: any;
@Component({ 
    selector: 'deploy-dialog',
    templateUrl: 'deploy_dialog.component.html' 
})
export class DeployDialog implements OnChanges {
    @Input() branch: string;
    @Input() repoName: string;
    @Input() commit_sha1: string;
    @Input() rollbackID: number;
    @Input() mode: string;

    lines: any = [];
    loading: boolean = false; 
    intervalID;

    constructor(private deployerService: DeployerService) { }

    ngOnInit() {
        this.deployerService.setRepoBranch(this.repoName, this.branch);
    }

    action() {
        this.loading = true;

        let promise = this.deployerService.deploy(this.commit_sha1)

        promise.subscribe(data => {
            this.loading = false;
        })
    }

    subscribeChannel()
    {
        let alerted = false;

        var pusher = new Pusher('73998387eb0020bf6d13', {
            cluster: 'us2'
        });

        var channel = pusher.subscribe(`logs.${this.commit_sha1.substring(0, 5)}`);
        channel.bind('new_log', (data) => {
            if(!alerted && data == 'finish'){
                alerted = true;
                alert('Deploy Finalizado')
                return;
            }

            if(data == 'finish') return;

            data.forEach(element => {
                this.lines.push(element.line);                
            });
        });
    }

    ngOnChanges(changes) {
        if(changes?.commit_sha1?.currentValue){
            this.subscribeChannel();
            this.deployerService.setRepoBranch(this.repoName, this.branch);
        }
    }
}