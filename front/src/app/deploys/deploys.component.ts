﻿import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { GitlabService} from '@app/_services';

@Component({ templateUrl: 'deploys.component.html' })
export class DeploysComponent {
    repoName: string;
    loading: boolean = true;
    branch = 'development';
    commits;
    commit_sha1;

    constructor(private route: ActivatedRoute, private router: Router, private gitlabService: GitlabService) { }

    ngOnInit() {
        this.repoName = this.route.snapshot.paramMap.get('repoName');
        this.getCommits(this.branch);

        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.repoName = this.route.snapshot.paramMap.get('repoName');
                this.getCommits(this.branch);
            }
        });
    }

    getCommits(branch) {
        this.branch = branch;
        this.loading = true;

        this.gitlabService.setRepoBranch(this.repoName, this.branch);
        this.gitlabService.getCommits().subscribe(data => {
            this.commits = data;
            this.loading = false;
        }, error => {});
    }
}