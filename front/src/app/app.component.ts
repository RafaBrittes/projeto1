﻿import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { AuthenticationService } from './_services';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    loggedIn: Boolean;
    isInLoginRoute: Boolean;
    currentUrl: string;
    repoName: string;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        if (localStorage.getItem('token')) this.loggedIn = true;
    }

    ngOnInit() {
        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                this.isInLoginRoute = this.router.url === '/login';
                this.currentUrl =  this.router.url;
            }
        });
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
        this.isInLoginRoute = true;
    }
}