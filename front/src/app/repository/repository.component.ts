import { Component, Input } from '@angular/core';
import { RepoBranchStateService } from '@app/_services';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'repository-card',
    templateUrl: 'repository.component.html',
})
export class RepositoryComponent {
    @Input() branch: string;
    @Input() repoName: string;
    repoBranchState: any;
    loading: boolean = true;

    constructor(private route: ActivatedRoute, private repoBranchService: RepoBranchStateService) {}

    ngOnInit() {
        this.repoBranchService.setRepoBranch(this.repoName, this.branch);
        this.repoBranchService.get().subscribe(data => {
            this.loading = false;
            this.repoBranchState = data;
        }, error => {});

        // this.router.events.subscribe((val) => {
        //     if (val instanceof NavigationEnd) {
        //         this.repoBranchService.setRepoBranch(this.repoName, this.branch);
        //         this.repoBranchService.get().subscribe(data => {
        //             this.loading = false;
        //             this.repoBranchState = data;
        //         }, error => {});
        //     }
        // });
    }

    getCommitLink() {
        return {
            'multipedidos': `https://gitlab.com/multipedidos/facepedidos/-/commit/${this.repoBranchState?.current_sha1}`
        }
    }

    toggleLock() {
        this.repoBranchService.toggleLock().subscribe();
    }
}