﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class DeployerService {
    repository: string;
    branch: string;
    url: string;

    constructor(private http: HttpClient) {}

    setRepoBranch(repository: string, branch: string) {
        this.repository = repository;
        this.branch = branch;

        this.url = `${environment.apiUrl}/${this.repository}/deploy/${this.branch}`;
    }

    deploy(commit_sha1 = null) {
        return this.http.get<any>(`${this.url}${commit_sha1 ? `/${commit_sha1}`:''}`)
    }
}