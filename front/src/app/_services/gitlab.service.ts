import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class GitlabService {
    repository: string;
    branch: string;
    url: string;

    constructor(private http: HttpClient) {}

    setRepoBranch(repository: string, branch: string) {
        this.repository = repository;
        this.branch = branch;

        this.url = `${environment.apiUrl}/${this.repository}/gitlab/${this.branch}/commits`;
    }

    getCommits() {
        return this.http.get<any>(this.url)
    }
}