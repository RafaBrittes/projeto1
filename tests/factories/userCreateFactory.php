<?php
namespace Database\Factories;

use App\Models\usersModels;
use Illuminate\Database\Eloquent\Factories\Factory;

class usersModelsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = usersModels::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->randomNumber($nbDigits = 5, $strict = false)
        ];
    }
}