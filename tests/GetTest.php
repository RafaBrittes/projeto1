<?php

use App\Models\usersModels as userModel;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Laravel\Lumen\Testing\WithoutMiddleware;
use App\userCreate;
class GetTest extends TestCase
{
    use DatabaseTransactions;
    use WithoutMiddleware;


    //public function setUp(): void
    //{
    //    parent::setUp();
    //
    //    $this->user = usersModels::create([
    //        'name' => 'Teste',
    //        'email' => 'teste@teste'
    //    ]);
    //}
    public $users;
    public function setUp(): void{
        parent::setUp();
        //$this->users = factory(userCreate::class)->make();
        // $this->users = factory(usersModels::class)->create();
        //dd($this->users->name);

        // factory(\usersModels::class)->create();
        $this->users = userModel::factory()->create();
    }


    public function testGetUsers()
    {
        $response = $this->json('GET', 'users');
        $response->seeStatusCode(200);

        $usersdecoded = json_decode($response->response->getContent());
        $this->assertEquals($this->users->name, $usersdecoded[count($usersdecoded) - 1]->name);
        
        // $this->$users->assertDatabaseCount('users', 5);
        // dd(count($usersdecoded));
        // $this->seeInDatabase('users', ['name' => $usersdecoded->name->end()]);
        // $seeuser = userModel::find($users->getContent());
        // $this->assertNotNull($response);
        // $this->assertEquals($this->user, $this->seeuser->only([$users->name]));
    }

    public function testPostCreate()
    {
        $response = $this->json('POST', 'users', [
            'name'  => $this->users->name,
            'email' => $this->users->email,
            'password' => $this->users->password
        ]);
        $response->seeStatusCode(200);

        $this->seeInDatabase('users', ['name' => $this->users->name]);
    }

    public function testChangeUser()
    {

        $response = $this->json('PUT', "users/{$this->users->id}", [
           'name'  => 'Changed', 
           'email' => 'changed@changed.com'
        ]);
        $response->seeStatusCode(200);

        $response = $this->json('GET', "users/{$this->users->id}");
        $response->seeStatusCode(200);
        //dd($response);
        $infos = json_decode($response->response->getContent());
        $this->assertEquals($infos->name, 'Changed');
        // $this->user->name = 'asd';
        // dd($this->user->name);
    }

    public function testUsersId()
    {
        $response = $this->json('GET', "users/{$this->users->id}");
        $response->seeStatusCode(200);

        $infos = json_decode($response->response->getContent());
        $this->assertEquals($infos->id, $this->users->id);
        $this->assertEquals($infos->name, $this->users->name);
    }

    public function testDeleteUser()
    {
        $response = $this->json('DELETE', "users/{$this->users->id}");
        $response->seeStatusCode(200);

        $this->notSeeInDatabase('users', ['id' => $this->users->id]);
    }
}